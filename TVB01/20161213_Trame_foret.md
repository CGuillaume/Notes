	# Point Trame for�t - 13.12.2016

## Objectifs
- R�capitulatif de la m�thode
- Valider la m�thode de passage des corridors machine aux corridors finaux
- Faire le point sur la suite du travail ONF / Livre Blanc, fiches m�thodes, etc.

## S�lection des secteurs
- On ajoute un secteur du 'du trefort' id 6

## Matrice de r�sistance
- La v2 de la matrice �tait un peu trop drastique avec les for�ts de moins bonnes qualit� et les plantations
	* cr�ation d'une nouvelle matrice en diminuant les r�sistances pour les milieux forestiers (v3)
	* Probl�mes pour le faire tourner, revoir avec des pixels un peu plus grand, fili�re SI d�fini la limite, valid� pour 12m
	* Faire des zones qui se superposent de 10%
	
## M�thode
- D�finition d'un seuil bas manuelement, par expertise
- Puis 6 classes automatiques
	* 1er � favorier en jaune
	* 5 autres � maintenir d�grad� de rose
- Coeur de biodiversit�
- Zone avec secteur � repecher trac�s par EF, puis s�lectionner les zones for�ts et milieux embroussaill�s

## Affichage final
- Polygones vecteur + d�grad� de couleur raster

## Validation par les partenaires
On fait un TP final par th�matique o� on balaye les �l�ments de la cat�gorie � renforcer puis les secteurs qui posent question.

	# R�troplanning
## Etape restantes
- Foret finir circuitscape - GC
- Zh refaire circuitscape avec nouveau coeur - GC
- Seuiller le courant pour chaque trame chaque image - RC
- Validation - EF
- Polygoniser avec classe la plus basse & les autres (jaune & rose) - GC
- Expertise modelage corridors (12,13 Janvier)- EF
- D�but Janvier vors les documents pour Lydie - GC
- R�cup�ration des zones � renforcer avec zones EF - GC
- TP final par th�matique o� on balaye les �l�ments de la cat�gorie � renforcer puis les secteurs qui posent question
- Produire un livrable cartographique
- Guide m�thode y compris les annexes et les fiches actions, plaquette synth�tique � finaliser

## Divers
- Saisir temps TVB pour le 22.12.2016

	
	
