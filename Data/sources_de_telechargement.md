# Sites de téléchargement des données publiques

> Il est souvent difficile de retrouver les données publiques : voici un petit catalogue repertoriant les hébergeurs.

## Données Sandre - (ONEMA, IRSTEA)
Service d'administration nationale des données et référentiel sur l'eau   
<http://www.sandre.eaufrance.fr/atlascatalogue/>

#### ROE (Obstacles à l'écoulement)
Mise à jour hebdomadaire en shape  
<http://services.sandre.eaufrance.fr/geo/obs_FXX?SERVICE=WFS&VERSION=1.1.0&REQUEST=GetFeature&typename=ObstEcoul&SRSNAME=EPSG:2154&OUTPUTFORMAT=SHAPEZIP>

## Agence de l'eau AERMC
<http://www.rhone-mediterranee.eaufrance.fr/telechargements/telechargements_carto.php>

## Région, Département, Arrondissement, EPCI, Commune, Chef Lieu - IGN ADMIN EXPRESS
<http://professionnels.ign.fr/adminexpress>
