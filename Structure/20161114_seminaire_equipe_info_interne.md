# 20161114 - Séminaire équipe - Les transferts d'information en interne

## Les --
- CR compte rendu réunion coordination,CA, Bureaux retour par service avec les points les concernants  = **Des comptes rendus existent déjà, réflexion sur la façon de faire redescendre l'information**
- Communication direction / chargée d'étude - projet
- Peu de détails sur qui fait quoi ? (Sauf les missions du pôle administratif récement)
  * Meilleur partage d'expérience
- Lien équipe - administrateur
  * Décision / Débats
  * Mieux se connaitre
- Lien intra équipe
  * Mutualiser
  * partage sur un sujet
  * Mieux se connaitre, convivialité
- Questions de comportement
  * Partage / Réflexion structure
- Plus de partage sur le lancement de pojets (interne notamment)
- Des retours rapides des CA / Bureaux et synthétiques
- Plus d'information sur les orientations stratégiques, les projets majeurs et les contextes réglementaires.
- Une infos prospective, sur la composition de l'équipe

## Les ++
- Communication cadre / chargée d'étude - projet, bonnne communication au sein d'équipe, intra filière
- Une bonne plateforme web technique / outils
- les echanges sur des feuilles thématiques
- Tableaux online

## Les pistes
- Retours réunion des coordination par service
- un répertoire des méthodes
- - de mails ? outils complémentaires
- Catégoriser les informations transmises
- Newsletter interne
- + de réunions d'équipe entière
  * Moins de TNA et plus d'échanges sont ils compatibles ?
- Homogénéisation des retours, cadres / équipes
- Une charte  comportementale sur le mode de partage / renseignement
- Une plateforme d'actu interne
- Rôle de la direction
- Des Stand-up  d'échanges très courts, hebdomadère, mensuel ?
  * Projets équipes, etc
  * Echelle antenne
- Intranet : Un outil d'échange interne (hors mails)

## Conclusion du matin
Besoin d'améliorer la communication qui déscend et la communication entre filières transversales.
Augmenter les échanges conviviaux, avec les administrateurs et au sein des équipes

- Responsabilisation de chacun à son niveau, la création d'une charte sur le comportement de recherche de l'infos et la diffusion
- Des Stand-up  d'échanges très courts, hebdomadaires, mensuels ?
  * Projets équipes, etc.
  * Echelle antenne
- Intranet : Un outil d'échange interne (hors mails)

## Retours aprem
- Avant plus d'echanges par mail notamment à la création d'un projet
- Pas trop jugé utile la création d'un outil échange interne
- Quel est le niveau d'information suffisant pour être efficace dans son travail et ne pas trop en demander ?
- Lettre interne ou les personnes qui jugent faire un projet utile au autres les font partagés
  * Peut initier ensuite des échanges ou rencontre en direct
- Structurer les équipes pour que la communication se fasse déjà à une petite échelle.
--> Savoir qui fait quoi et quels sont les compétences de chacun...
- Soliciter les administrateurs sur des moments fort de la vie d'un site, faire appel à eux pour qui nous représente dans les projets
- recréer du lien avec administrateurs
  * cause : membre du CA sont salariés, réintégrer des bénévoles ? avoir des référents thématiques
- rédéfinir ce que l'on attend de chacun, CE, CP, direction, CA, Conseil scientifique


---
# Les transferts d'information en interne
### A l'échelle de l'individu
- Avoir la CEN attitude : avoir le réflexe de chercher l'info et de la partager. D'utiliser les compétences des collègues et de co-construire.
  * Création et application d'une Charte

### A l'échelle de la filière
- En tant que responsable ne pas oublier d'informer son équipe.
- Homogénéiser la communication au sein des filières / antennes
- Sélectionner les infos pertinentes pour la structure

### A l'échelle de l'antenne
- Des Stand-up  d'échanges très courts, hebdomadaires, mensuels ?
  * Projets équipes, nouveaux projets, actualité, etc.
- Sélectionner les infos pertinentes pour la structure

### A l'échelle de la structure
- Créer un Qui Fait Quoi ? Tableau ONLINE !
- Réfléchir à un outil qui permet d'améliorer la communication interne --> Groupe de travail
  * lettre d'infos
  * intranet
  * etc.
- Récréer du lien avec les administrateurs
  * moment de convivialité
  * bénévoles dans le CA, plus disponibles
  * référents thématiques

### A l'échelle de la direction
- Affirmer son rôle sur une synthèse rapide des infos et décisions prises en bureaux, CA et autres instances

### Ouverture
- Importance de la communication au sein du réseau CEN et autres
