# CTT SIG - Séminaire Sète - 14.10.2016
## Base de données Contact - CEN NPDC
  Volonté d'harmoniser les données et de les sécuriser
- Gestion des adhésions, envoi de reçut fiscaux
- Publipostage
- Gestion des bénévoles
- 1000€ pour la récupérer
- Volonté de créer un groupe d'utilisateur pour les dévellopement futurs

#### Fonction
- Répertoire
- Recherches simple et multicritère
- Export csv
- Publipostage
- Import de contact en masse

#### Démo
Correction du bug et ajout de la fonctionnalité, pour les personnes avec plusieurs catégories / adresse

## Base Mare - CEN Haute Normandie (Charles)
Base web avec saisie en ligne.  
Une partie tronc commun de la base ce qui fait qu'elle peut être utiliser pour d'autres thématiques.  
Complètement récupérable.

## Demande de la fédération - Nouvelle chargée de Communication (POIRIER Sandrine)
La fédération demande si nous pouvons être mobiliser sur une carte de valorisation des sites. Cartes accès sur les sites emblématiques et adaptès à l'acceuil du grand public.
Les infos et contours de sites sont remontés tous les ans en juin à l'aide du tableau de bord.  
Réflexion sur le fait d'utiliser le module cartographique des sites fédéraux alimenter par les données du tableau de bords.
La fédé n'a qu'une prestation à faire pour mettre en place le module carto et les géomaticiens pas plus de boulot.  
Exemple : <http://conservatoirepicardie.org/sites-et-milieux-naturels>

## BDD Habitat - CEN Picardie (Gratien)
- Utilise un référentiel habitat du CEN de Brest  
Un rapprochement est effectué avec les autres référentiels, c'est un référentiel plus simple à utiliser.
- Ouvre un interface Access depuis QGIS
- 2 Symbologie automatique postgres
	 * A la division
	 * au macro groupe
- Modèle de mise en page automatique avec en 2 ème page un tableau de donnée

## WebObs - CEN MidiPy (Laurent)
<http://webobs.cen-mp.org/>  
Outils de reporting des données d'observation avec une vision communautaire  
Utilise un fichier json formaté issu de SICEN ou autre bases
Déployable pour n'importe qu'elle emprise

## Feuille de route 2017
- Transformer la journée CTT SIG du forum TIC en une journée de travail collaboratif
- Mise de la formation ODK au catalogue de l'ATEN
- Réaliser un listing des outils mis en place dans les CENs

## Les plus
Service d'adhésions : helloasso (Charles)
<https://www.helloasso.com/>  
Projet d'ERP au CEN Aquitaine (Mickael)  
Librairie php pour écrire des docs words, à utiliser pour la refonte de la base à Manu, la création de rapport  
Export des données SICEN sur un site : 200 m sur la faune et 5 m sur la flore
