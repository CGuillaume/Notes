# CTT 4 - Espèces exotiques envahisssantes

# Bassin de la Loire
Stratégie 2014-2020
> Améliorer la prévention, la gestion et la sensibilisation (Faune et Flore)
En ce moment mise en oeuvre de la stratégie

## Perspective
- Suivi de la stratégie
- Mise en place d'outils 
- Etc.

## La stratégie
Première stratégie à l'échelle d'un bassin.
Réflexion pour une stratégie à l'échelle nationale.

# Stratégie régionale NPDC
Hierachisation des especes avec une méthode Belge ISEIA, basé sur :
Travail fait sur la faune mais pas sur la flore
-  Potentiel reproducteur
- Potentiel de dispersion
- Transmission des patogènes 
- Etc.
Une note de 1 à 3 par critère, puis on obtient une note finale qui défini le niveau d'invasion et l'impact environemental

Stratégie d'action à l'échelle régionales (une dizaine)
- Etat de l'existant
- Communicatiin
- Etc.
Avec le changement de région ça change pour une stratégie nationale et le calendrier est un peu bloqué.

## Difficultés
Pas une priorité auprés de la DREAL
Difficultés pour mobiliser des partenaire (insecte, mollusque)
